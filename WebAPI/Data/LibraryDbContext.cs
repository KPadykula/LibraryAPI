﻿using WebAPI.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace WebAPI.Data
{
    public class LibraryDbContext : DbContext
    {
        public LibraryDbContext()
        {

        }
        public LibraryDbContext(DbContextOptions<LibraryDbContext> options) : base(options) { }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Book>()
                .HasOne<Category>()
                .WithMany()
                .HasForeignKey(b => b.id_category);
        }

        public virtual DbSet<Category> category { get; set; }
        public virtual DbSet<Book> book { get; set; }
    }
}
