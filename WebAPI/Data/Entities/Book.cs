﻿using System.ComponentModel.DataAnnotations;

namespace WebAPI.Data.Entities
{
    public class Book
    {
        public int id { get; set; }       
        [Required(ErrorMessage = "Title is required")]
        public string title { get; set; }    
        public string? author { get; set; }
        [Required(ErrorMessage = "id category is required")]
        public int id_category { get; set; }
    }
}
