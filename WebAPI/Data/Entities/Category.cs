﻿
using System.ComponentModel.DataAnnotations;

namespace WebAPI.Data.Entities
{
    public class Category
    {
        public int id { get; set; }
        [Required(ErrorMessage = "name is required")]
        public string name { get; set; }
    }
}
