﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Data;
using WebAPI.Data.Entities;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BookController : ControllerBase
    {
        private readonly LibraryDbContext _libraryDbContext;

        public BookController(LibraryDbContext libraryDbContext) 
            => _libraryDbContext = libraryDbContext;
        

        [HttpGet]
        public async Task<IActionResult> GetBookAsync()
        {
            var bookToReturn = await _libraryDbContext.book.ToListAsync();
            return Ok(bookToReturn);
        }

        [HttpGet]
        [Route("Get-Book-ByID")]
        public async Task<IActionResult> GetBookByIdAsync(int id)
        {
            var bookToReturn = await _libraryDbContext.book.FindAsync(id);
            return Ok(bookToReturn);
        }
        
        [HttpGet]
        [Route("Get-Book-ByCategory")]
        public async Task<IActionResult> GetBookByCategory(string categoryName)
        {
            var categoryToSearch = await _libraryDbContext.category.FirstOrDefaultAsync(x => x.name.ToLower() == categoryName.ToLower());
            var listOfBooksInDb = await _libraryDbContext.book.ToListAsync();
            var listOfBooksToReturn = new List<Book>();
           
            foreach (var book in listOfBooksInDb)
            {
                if (categoryToSearch != null)
                    if (book.id_category == categoryToSearch.id) listOfBooksToReturn.Add(book);
            }
            return listOfBooksToReturn.Count > 0 ? Ok(listOfBooksToReturn) : NotFound();
        }
        
        [HttpPost]
        public async Task<IActionResult> PostBookAsync(Book book)
        {
            _libraryDbContext.book.Add(book);
            await _libraryDbContext.SaveChangesAsync();

            return Created($"/Get-Book-ByID?id={book.id}", book);
        }

        [HttpPut]
        public async Task<IActionResult> PutAsync(Book book)
        {
            _libraryDbContext.book.Update(book);
            await _libraryDbContext.SaveChangesAsync();
            return NoContent();
        }

        [Route("{id}")]
        [HttpDelete]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            var bookToDelete = await _libraryDbContext.book.FindAsync(id);

            if (bookToDelete == null)
            {
                return NotFound();
            }

            _libraryDbContext.book.Remove(bookToDelete);
            await _libraryDbContext.SaveChangesAsync();

            return NoContent();
        }
    }
}
