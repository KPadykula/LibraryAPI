﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Data;
using WebAPI.Data.Entities;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CategoryController : ControllerBase
    {
        private readonly LibraryDbContext _libraryDbContext;

        public CategoryController(LibraryDbContext libraryDbContext) 
            => _libraryDbContext = libraryDbContext;

        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            var category = await _libraryDbContext.category.ToListAsync();
            return Ok(category);
        }

        [HttpGet]
        [Route("Get-Category-ByID")]
        public async Task<IActionResult> GetCategoryByIdAsync(int id)
        {
            var category = await _libraryDbContext.category.FindAsync(id);
            return Ok(category);
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync(Category category)
        {
            _libraryDbContext.category.Add(category);
            await _libraryDbContext.SaveChangesAsync();

            return Created($"/Get-Category-ByID?id={category.id}", category);
        }

        [HttpPut]
        public async Task<IActionResult> PutAsync(Category category)
        {
            _libraryDbContext.category.Update(category);
            await _libraryDbContext.SaveChangesAsync();
            return NoContent();
        }

        [Route("{id}")]
        [HttpDelete]
        public async Task<IActionResult> DeleteAsync(int id) 
        {
            var categoryToDelete = await _libraryDbContext.category.FindAsync(id);

            if (categoryToDelete == null)
            {
                return NotFound();
            }
            
            _libraryDbContext.category.Remove(categoryToDelete);
            await _libraryDbContext.SaveChangesAsync();

            return NoContent();
        }
    }
}
